unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls;

type
  TForm1 = class(TForm)
    imagem_rnb: TImage;
    inserir_aqui: TEdit;
    lista: TListBox;
    btn_del: TButton;
    btn_att: TButton;
    btn_inserir: TButton;
    btn_oque: TButton;
    title: TLabel;
    btn_rnb: TButton;
    adicione: TLabel;
    btn_save: TButton;
    artistas: TButton;
    btn_txt: TButton;
    procedure btn_delClick(Sender: TObject);
    procedure btn_inserirClick(Sender: TObject);
    procedure btn_oqueClick(Sender: TObject);
    procedure btn_attClick(Sender: TObject);
    procedure btn_rnbClick(Sender: TObject);
    procedure artistasClick(Sender: TObject);
    procedure btn_saveClick(Sender: TObject);
    procedure btn_txtClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

   TMusicos = class(TObject)
   nome:string;
   end;

var
   Form1: TForm1;
   banda_rnb: TMusicos;
   arquivo: TextFile;

implementation

{$R *.dfm}

procedure TForm1.btn_delClick(Sender: TObject);
begin
lista.Items.Delete(lista.ItemIndex);
end;

procedure TForm1.btn_inserirClick(Sender: TObject);
begin
banda_rnb := TMusicos.create;

banda_rnb.nome:=inserir_aqui.Text;

lista.Items.AddObject(banda_rnb.nome, banda_rnb);
end;

procedure TForm1.btn_oqueClick(Sender: TObject);
begin
ShowMessage('A m�sica � uma forma de arte que se constitui na combina��o de v�rios sons e ritmos, seguindo uma pr�-organiza��o ao longo do tempo. � considerada por diversos autores como uma pr�tica cultural e humana. ');
end;

procedure TForm1.btn_attClick(Sender: TObject);
begin
lista.Items[lista.ItemIndex] := inserir_aqui.Text;
end;

procedure TForm1.btn_rnbClick(Sender: TObject);
begin
ShowMessage('Rhythm and blues ou R&&B � um termo comercial introduzido nos Estados Unidos no final da d�cada de 1940 pela revista Billboard.');
end;

procedure TForm1.artistasClick(Sender: TObject);
begin
ShowMessage('Kehlani - The Weeknd - Years && Years - The Temptations - Luther Vandross - Diana Ross - Aaliyah.');
end;

procedure TForm1.btn_saveClick(Sender: TObject);
begin
banda_rnb:=lista.Items.Objects[lista.ItemIndex] as TMusicos;
ShowMessage('gravado.');

AssignFile(arquivo, 'save_here.txt');
Rewrite(arquivo);
WriteLn(arquivo, inserir_aqui.Text);

CloseFile(arquivo);
end;

procedure TForm1.btn_txtClick(Sender: TObject);
var musicos: TMusicos;
    arquivo: TextFile;
begin
AssignFile(arquivo, 'save_here.txt') ;
Reset(arquivo);

while not Eof(arquivo) do
begin
musicos:=TMusicos.Create();
ReadLn(arquivo, musicos.nome);

lista.Items.AddObject(musicos.nome, musicos);
end;
CloseFile(arquivo);
end;
end.
